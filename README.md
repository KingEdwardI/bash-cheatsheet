# Bash Cheatsheet

## :warning: :warning: :warning: This project is still under heavy development. 

Not everything will work as expected and it is far from being in a very useful state.

## Introduction

A simple program to print a cheatsheet of various bash commands.

**Please Note:** These commands are bash-specific. Though some of them
may run on multiple shells, others will not, especially on non-feature-rich
shells like `sh`.

Basically I'm really bad at remembering some bash commands, and I
don't like having to go search them up all the time. So I've
compiled a small program that will simply print out some examples
of various items.

I basically want a clone of this website: https://devhints.io/bash
but text-only, and logically organized so it's easy to find any given
command.

## Getting started

- Todo: an official way to distribute the executable.

For now, you can simply download the `bcs-*` file for your OS and add that to your path.

Todo: specific instructions for each operating system.

## Coming soon

First I want to get a handful of what I find most useful written out - one of the most time consuming things of this project
is simply going to be getting all of the information organized nicely. Formatting things nicely isn't easy in the terminal.

After that, the immediate next step will be to turn this into a binary executable - It should be very easy to install and use
The purpose is for it to be accessible to all levels of people using bash.

As a big stretch goal, I want to make an interactive TUI so it's easy to navigate through the different
commands that you might not be familiar with. 

- https://github.com/chjj/blessed

At some point, I'd really like to clean up the formatting of the conditional tables (and any others that I end up making).
They work for now, but they're way too fragile.

- Maybe just have a formatted file and print that directly to the terminal? Am I fucking stupid?
    - Okay, that's not entirely plausible - trying to do that with the colors might fuck shit up.
    - But for the simpler ones... That might be the way to go. I just need the information in a nice printed format.


## Todo: Finish up the following "docs"

### v0.10

- [x] colors
- [x] basic conditionals
- [x] functions
- [x] loops
- [ ] string basics (declaring, substituting, slicing)
- [ ] variables
- [ ] working cli

### Next (no priority order)

- Numbers & math
- Switch/case statements (conditionals)
- Advanced strings (transformations, manipulation, substitution, substrings, anything I didn't already cover)
- Arrays & Dictionaries

### Later

- printf (ffs nooooo)
- Getting options (i.e. for a cli)
- Explain subshells
- Common builtin bash variables
