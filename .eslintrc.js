module.exports = {
  env: {
    es6: true,
    node: true,
  },
  extends: ['eslint:recommended', 'plugin:prettier/recommended'],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  plugins: ['sort-keys-fix', 'prettier'],
  root: true,
  rules: {
    // indent: ['error', 2],

    'max-len': ['error', 100, { ignoreStrings: true, ignoreTemplateLiterals: true }],
    // 'import/order': [
    //   'error',
    //   {
    //     'alphabetize': { 'caseInsensitive': true, 'order': 'asc' },
    //     groups: [
    //       'external',
    //       'builtin',
    //       'internal',
    //       'parent',
    //       'sibling',
    //       'index'
    //     ],
    //     'newlines-between': 'always'
    //   }
    // ],
    'prettier/prettier': [
      'error',
      {
        arrowParens: 'avoid',
        endOfLine: 'auto',
        printWidth: 100,
        singleQuote: true,
        trailingCommas: 'all',
      },
    ],
    'quote-props': [2, 'as-needed'],
    semi: ['error', 'always'],
    'sort-imports': [
      'error',
      {
        allowSeparatedGroups: true,
        ignoreCase: true,
        ignoreDeclarationSort: true,
        ignoreMemberSort: false,
        memberSyntaxSortOrder: ['none', 'all', 'multiple', 'single'],
      },
    ],
    'sort-keys': 'off',
    'sort-keys-fix/sort-keys-fix': 'error',
  },
};
