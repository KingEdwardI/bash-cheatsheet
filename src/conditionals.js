const { paddings, lines, p, borders } = require('./formattingUtils');
const { log } = require('./utils');

const { Tb, Tl, Tr, Tt, bl, br, t, tl, tr, v } = borders;

const basicConditions = [
  {
    comparator: '==',
    description: 'Equal',
    lhs: 'STRING',
    rhs: 'STRING',
  },
  {
    comparator: '!=',
    description: 'Not equal',
    lhs: 'STRING',
    rhs: 'STRING',
  },
  {
    comparator: '-z',
    description: 'Empty string',
    lhs: '',
    rhs: 'STRING',
  },
  {
    comparator: '-n',
    description: 'Non-empty string',
    lhs: '',
    rhs: 'STRING',
  },
  {
    comparator: '-eq',
    description: 'Equal',
    lhs: 'NUM',
    rhs: 'NUM',
  },
  {
    comparator: '-ne',
    description: 'Not equal',
    lhs: 'NUM',
    rhs: 'NUM',
  },
  {
    comparator: '-lt',
    description: 'Less than',
    lhs: 'NUM',
    rhs: 'NUM',
  },
  {
    comparator: '-gt',
    description: 'Greater than',
    lhs: 'NUM',
    rhs: 'NUM',
  },
  {
    comparator: '-le',
    description: 'Less than or equal',
    lhs: 'NUM',
    rhs: 'NUM',
  },
  {
    comparator: '-ge',
    description: 'Greater than or equal',
    lhs: 'NUM',
    rhs: 'NUM',
  },
  {
    comparator: '=~',
    description: 'Regex comparison',
    lhs: 'STRING',
    rhs: 'STRING',
  },
  {
    comparator: '!',
    description: 'Not',
    lhs: '',
    rhs: 'EXPRESSION',
  },
  {
    comparator: '&&',
    description: 'And',
    lhs: 'X',
    rhs: 'Y',
  },
  {
    comparator: '||',
    description: 'Or',
    lhs: 'X',
    rhs: 'Y',
  },
];

const fileConditions = [
  {
    comparator: '-e',
    description: 'Exists',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-r',
    description: 'Readable',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-h',
    description: 'Symlink',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-d',
    description: 'Directory',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-w',
    description: 'Writable',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-s',
    description: 'Size is > 0 bytes',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-f',
    description: 'File',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-x',
    description: 'Executable',
    lhs: '',
    rhs: 'FILE',
  },
  {
    comparator: '-nt',
    description: '1 is more recent than 2',
    lhs: 'FILE1',
    rhs: 'FILE2',
  },
  {
    comparator: '-ot',
    description: '2 is more recent than 1',
    lhs: 'FILE1',
    rhs: 'FILE2',
  },
  {
    comparator: '-ef',
    description: 'Same files',
    lhs: 'FILE1',
    rhs: 'FILE2',
  },
];

const { p2, p3, p5, p9, p10, p15, p60 } = paddings;
const { l28, l30 } = lines;

// Holy motherfucker this is one fucked up way to build and print tables...
// it's extremely fragile, just never touch this shit ever again
const printConditionalTable = () => {
  const basicTitle = `${p3}Basic Conditions`;
  const fileTitle = 'File Conditions';
  const titleLine = `${basicTitle}${p60}${fileTitle}`;
  log(titleLine, '\n');

  // Condition (28) | Description (30)
  const tableHeaderTopLine = `${tl}${l28}${Tt}${l30}${tr}`;
  const tableHeaderFullTopLine = `${p5}${tableHeaderTopLine}${p15}${tableHeaderTopLine}`;
  log(tableHeaderFullTopLine);

  const tableHeaderText = `${v}${p9}Condition${p10}${v}${p9}Description${p10}${v}`;
  const tableHeaderFullCells = `${p5}${tableHeaderText}${p15}${tableHeaderText}`;
  log(tableHeaderFullCells);

  const tableHeaderBottomLine = `${Tl}${l28}${t}${l30}${Tr}`;
  const tableHeaderFullBottomLine = `${p5}${tableHeaderBottomLine}${p15}${tableHeaderBottomLine}`;

  log(tableHeaderFullBottomLine);
  basicConditions.forEach((conditional, i) => {
    const { lhs, rhs, comparator: c, description: d } = conditional;

    const basicCondition = `${p5}${v}${p2}[[ ${lhs && `${lhs} `}${c} ${rhs} ]]`;
    const conditionPad = p(30 - basicCondition.length + 4);
    const descriptionPad = p(28 - d.length);
    const basicRow = `${basicCondition}${conditionPad}${v}${p(2)}${d}${descriptionPad}${v}`;
    let row = basicRow;

    if (i < fileConditions.length) {
      const { lhs: fLhs, rhs: fRhs, comparator: fc, description: fd } = fileConditions[i];
      const fileCondition = `${p2}[[ ${fLhs && `${fLhs} `}${fc} ${fRhs} ]]`;
      const fConditionPad = p(28 - fileCondition.length);
      const fDescriptionPad = p(28 - fd.length);
      const fileRow = `${v}${fileCondition}${fConditionPad}${v}${p2}${fd}${fDescriptionPad}${v}`;
      row = `${basicRow}${p15}${fileRow}`;
    }
    log(row);

    const tableLine = `${Tl}${l28}${t}${l30}${Tr}`;
    const tableBottomLine = `${bl}${l28}${Tb}${l30}${br}`;

    const tableFullLineWithFile = `${p5}${tableLine}${p15}${tableLine}`;
    const tableFullLineWithoutFile = `${p5}${tableLine}`;

    const tableFullBottomLineWithFile = `${p5}${tableLine}${p15}${tableBottomLine}`;
    const tableFullBottomLineWithoutFile = `${p5}${tableBottomLine}`;

    if (i < basicConditions.length && i < fileConditions.length - 1) {
      log(tableFullLineWithFile);
      return;
    }
    if (i < basicConditions.length && i === fileConditions.length - 1) {
      log(tableFullBottomLineWithFile);
      return;
    }
    if (i < basicConditions.length - 1) {
      log(tableFullLineWithoutFile);
      return;
    }
    if (i === basicConditions.length - 1) {
      log(tableFullBottomLineWithoutFile);
      return;
    }
  });
};

// Todo: Switch/Case statements.

printConditionalTable();

