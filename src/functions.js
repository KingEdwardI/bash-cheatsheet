const functionBasics = `
Functions

Defining functions

There are two ways to define functions in bash, the effect is the same but the
syntax is slightly different.

myFunc() {
  echo "Hello World"
}

You would call this method like so:

$ myFunc
> Hello World


Function Parameters

Parameters are passed to functions positionally, so you can access them by referencing
the index of the parameter you're trying to use.

myFunc() {
  echo "Hello $1"
}

$ myFunc John
> Hello John

Or with multiple arguments.

myFunc() {
  echo "Hello $1"
  echo "Your favorite food is $2"
}

$ myFunc John Pizza
> Hello John
> Your favorite food is Pizza


Return Statements & Returning Values

In bash, returning a value from a function is a bit different than other languages.
While there is a \`return\` statement, it's not used to return a value - \`return\`
will just exit out of a function while not exiting the program. Here's how that
works.

If no arguments are provided to this function, then it will print out "Need one argument!"
Otherwise it'll print "Doin' some stuff with <arg>""

myFunc() {
  if [[ -z "$1" ]]; then
    echo "Need one argument!"
    return;
  fi
  echo "Doin' some stuff with $1"
}

$ myFunc
> Need one argument!

$ myFunc pizza
> Doin' some stuff with pizza

Instead of using a classic return statement, Bash takes advantage of the built
in I/O. So what you need to do is \`echo\` the value that you want to use, 
which will send it as output to the calling method.

add() {
  # Don't worry about the arithmetic syntax here
  echo $(($1 + $2))
}

printOnePlusTwo() {
  result=$(add 1 2)
  echo "$result"
}
`;

const argumentsTable = ``;
