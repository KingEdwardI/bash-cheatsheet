const yargs = require('yargs');

const { printAnsiExplanation } = require('./colors');

yargs
  .scriptName('bcs')
  .command(
    'colors',
    'How to use colors in bash',
    yarg => {
      yarg.positional('ansi', {
        describe: 'Ansi explanation',
        type: 'boolean',
      });
      yarg.positional('all-colors', {
        describe: `Print all colors with their color codes. 
        Note: you may have to highlight the text to see the code.`,
        type: 'boolean',
      });
      yarg.positional('table', {
        describe: 'Table with information about all colors',
        type: 'boolean',
      });
      yarg.positional('foreground', {
        alias: 'fg',
        describe: 'Print all foreground colors',
        type: 'boolean',
      });
      yarg.positional('background', {
        alias: 'bg',
        describe: 'Print all background colors',
        type: 'boolean',
      });
      yarg.positional('styles', {
        alias: 'styl',
        describe: 'Print all styles',
      });
    },
    () => {
      printAnsiExplanation();
    }
  )
  .help().argv;
