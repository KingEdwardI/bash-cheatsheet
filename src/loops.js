const { log, addLeftPadding } = require('./formattingUtils');

const basicForLoop = addLeftPadding(`
Basic for loops can be used on all sorts of different things. From listing files,
to reading lines of a file, or just over an arbitrary range.

Loop over a list of files in a folder.
This is a contrived example, it just emulates what \`ls\` does.

# Note the \`*\` after the \`/\`. This tells it to grab all the files
# in the folder, otherwise it's just referencing the folder itself   
for i in /*; do 
  echo "$i" # \`i\` will be a file or folder name
done

> /var
> /bin
> /tmp
> /home
> # ...etc
`);
const printBasicForLoop = () => log(basicForLoop);

const c_LikeForLoop = addLeftPadding(`
C-like for loop.

# Loop over the numbers 0-9 and print the number
for ((i = 0 ; i < 10 ; i++)); do
  echo "$i"
done

> 0
> 1
> 2
> 3
> # ...etc
`);
const printC_LikeForLoop = () => log(c_LikeForLoop);

const rangeLoops = addLeftPadding(`
Looping over ranges.
This is similar to the C-like loop.

# This will loop over the numbers 1-5 and print \`Welcome <number>\`
for i in {1..5}; do
  echo "Welcome $i"
done

> Welcome 1
> Welcome 2
> # ...etc

# You can also add a step count to skip over numbers
# This will print every fifth number from 5-50
for i in {5..50..5}; do
  echo "Welcome $i"
done

> Welcome 5
> Welcome 10
> # ...etc
`);
const printRangeLoops = () => log(rangeLoops);

const whileLoops = addLeftPadding(`
While loops.

# This will read the lines of a given file and print each line.
# \`read\` is a method that will take input from the command line, commonly from a user.
# In this case we are supplying \`file.txt\` as that input via the \`<\` operator.
while read -r line; do
  echo "$line"
done <file.txt 

# You can also have a while loop that runs forever
while true; do
  # do some stuff here
done
`);
const printWhileLoops = () => log(whileLoops);

module.exports = {
  printBasicForLoop,
  printC_LikeForLoop,
  printRangeLoops,
  printWhileLoops,
};
