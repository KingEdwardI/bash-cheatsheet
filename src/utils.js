/* eslint-env node */

const { log } = console;

const styles = {
  bold: {
    code: '1',
    description: 'Bold text',
    example: '\033[1mExample\033[0m',
    name: 'Bold',
    sequence: '\\e[1m',
  },
  inverted: {
    code: '7',
    description: 'Invert text colors. Foreground becomes background & vice versa.',
    example: '\033[7mExample\033[0m',
    name: 'Invert',
    sequence: '\\e[7m',
  },
  italic: {
    code: '3',
    description: 'Italic text',
    example: '\033[3mExample\033[0m',
    name: 'Italic',
    sequence: '\\e[3m',
  },
  lightGrey: {
    code: '2',
    description: 'Dim text (greyish)',
    example: '\033[2mExample\033[0m',
    name: 'Light Grey',
    sequence: '\\e[2m',
  },
  reset: {
    code: '0',
    description: 'Reset all attributes (text color, background color, and style)',
    example: 'Example',
    name: 'Reset',
    sequence: '\\e[0m',
  },
  strikethrough: {
    code: '9',
    description: 'Strikethrough text',
    example: '\033[9mExample\033[0m',
    name: 'Strikethrough',
    sequence: '\\e[9m',
  },
  underline: {
    code: '4',
    description: 'Underline text',
    example: '\033[4mExample\033[0m',
    name: 'Underline',
    sequence: '\\e[4m',
  },
};

const foreground = {
  blackFg: {
    code: '30',
    description: 'Black foreground',
    example: '\033[30;7mExample\033[0m',
    name: 'Black',
    sequence: '\\e[30m',
  },
  blueFg: {
    code: '34',
    description: 'Blue foreground',
    example: '\033[34mExample\033[0m',
    name: 'Blue',
    sequence: '\\e[34m',
  },
  cyanFg: {
    code: '36',
    description: 'Cyan foreground',
    example: '\033[36mExample\033[0m',
    name: 'Cyan',
    sequence: '\\e[36m',
  },
  greenFg: {
    code: '32',
    description: 'Green foreground',
    example: '\033[32mExample\033[0m',
    name: 'Green',
    sequence: '\\e[32m',
  },
  magentaFg: {
    code: '35',
    description: 'Magenta foreground',
    example: '\033[35mExample\033[0m',
    name: 'Magenta',
    sequence: '\\e[35m',
  },
  redFg: {
    code: '31',
    description: 'Red foreground',
    example: '\033[31mExample\033[0m',
    name: 'Red',
    sequence: '\\e[31m',
  },
  whiteFg: {
    code: '37',
    description: 'White foreground',
    example: '\033[37mExample\033[0m',
    name: 'White',
    sequence: '\\e[37m',
  },
  yellowFg: {
    code: '33',
    description: 'Yellow foreground',
    example: '\033[33mExample\033[0m',
    name: 'Yellow',
    sequence: '\\e[33m',
  },
};

const foregroundAlt = {
  lightBlueFg: {
    code: '94',
    description: 'Light Blue foreground',
    example: '\033[94mExample\033[0m',
    name: 'Blue',
    sequence: '\\e[94m',
  },
  lightCyanFg: {
    code: '96',
    description: 'Light Cyan foreground',
    example: '\033[96mExample\033[0m',
    name: 'Cyan',
    sequence: '\\e[96m',
  },
  lightGreenFg: {
    code: '92',
    description: 'Light Green foreground',
    example: '\033[92mExample\033[0m',
    name: 'Green',
    sequence: '\\e[92m',
  },
  lightGreyFg: {
    code: '90',
    description: 'Light Grey foreground',
    example: '\033[90mExample\033[0m',
    name: 'Grey',
    sequence: '\\e[90m',
  },
  lightMagentaFg: {
    code: '95',
    description: 'Light Magenta foreground',
    example: '\033[95mExample\033[0m',
    name: 'Magenta',
    sequence: '\\e[95m',
  },
  lightRedFg: {
    code: '91',
    description: 'Light Red foreground',
    example: '\033[91mExample\033[0m',
    name: 'Red',
    sequence: '\\e[91m',
  },
  lightYellowFg: {
    code: '93',
    description: 'Light Yellow foreground',
    example: '\033[93mExample\033[0m',
    name: 'Yellow',
    sequence: '\\e[93m',
  },
  whiteFgAlt: {
    code: '97',
    description: 'Light White foreground',
    example: '\033[97mExample\033[0m',
    name: 'White',
    sequence: '\\e[97m',
  },
};
const background = {
  blackBg: {
    code: '40',
    description: 'Black background',
    example: '\033[40mExample\033[0m',
    name: 'Black',
    sequence: '\\e[40m',
  },
  blueBg: {
    code: '44',
    description: 'Blue background',
    example: '\033[44mExample\033[0m',
    name: 'Blue',
    sequence: '\\e[44m',
  },
  cyanBg: {
    code: '46',
    description: 'Cyan background',
    example: '\033[46mExample\033[0m',
    name: 'Cyan',
    sequence: '\\e[46m',
  },
  greenBg: {
    code: '42',
    description: 'Green background',
    example: '\033[42mExample\033[0m',
    name: 'Green',
    sequence: '\\e[42m',
  },
  magentaBg: {
    code: '45',
    description: 'Magenta background',
    example: '\033[45mExample\033[0m',
    name: 'Magenta',
    sequence: '\\e[45m',
  },
  redBg: {
    code: '41',
    description: 'Red background',
    example: '\033[41mExample\033[0m',
    name: 'Red',
    sequence: '\\e[41m',
  },
  whiteBg: {
    code: '47',
    description: 'White background',
    example: '\033[47mExample\033[0m',
    name: 'White',
    sequence: '\\e[47m',
  },
  yellowBg: {
    code: '43',
    description: 'Yellow background',
    example: '\033[43mExample\033[0m',
    name: 'Yellow',
    sequence: '\\e[43m',
  },
};
const colors = {
  ...styles,
  ...foreground,
  ...foregroundAlt,
  ...background,
};

module.exports = { colors, log };
