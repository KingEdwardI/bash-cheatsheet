/* eslint-env node */

const { log, colors } = require('./utils');
const { paddings, lines, fmt, p, borders, addLeftPadding } = require('./formattingUtils');

const { Tb, Tl, Tr, Tt, bl, br, t, tl, tr, v } = borders;

const { p3, p5, p7, p25, p34 } = paddings;
const { l10, l13, l14, l18, l70 } = lines;

const alp = addLeftPadding;

const printColorTable = () => {
  // Name (18) | Description (70) | Code (10) | Sequence (14) | Example (13)
  const tableHeader = alp(`
${tl}${l18}${Tt}${l70}${Tt}${l10}${Tt}${l14}${Tt}${l13}${tr}
${v}${p7}Name${p7}${v}${p25}Description${p34}${v}${p3}Code${p3}${v}${p3}Sequence${p3}${v}${p3}Example${p3}${v}
${Tl}${l18}${t}${l70}${t}${l10}${t}${l14}${t}${l13}${Tr}`);
  log(tableHeader);
  Object.values(colors)
    .sort((a, b) => Number(a.code) - Number(b.code))
    .forEach(color => {
      const { name, description: desc, code, sequence: seq, example: ex } = color;
      const adjNamePad = 18 - color.name.length - 3;
      const adjDescPad = 70 - color.description.length - 3;
      const adjCodePad = 10 - color.code.length - 3;
      const adjSeqPad = 14 - color.sequence.length - 3;

      const nameCell = `${p3}${name}${p(adjNamePad)}`;
      const descCell = `${p3}${desc}${p(adjDescPad)}`;
      const codeCell = `${p3}${code}${p(adjCodePad)}`;
      const seqCell = `${p3}${seq}${p(adjSeqPad)}`;
      const exampleCell = `${p3}${ex}${p3}`;

      const cell = alp(
        `${v}${nameCell}${v}${descCell}${v}${codeCell}${v}${seqCell}${v}${exampleCell}${v}`
      );
      log(cell);
      // Todo: Not sure if I like having lines between table cells.
      // if (i < Object.values(colors).length - 1) {
      //   log(
      //     `${p5}${Tl}${l18}${t}${l70}${t}${l10}${t}${l14}${t}${l13}${Tr}`
      //   );
      // }
    });
  const tableBottom = alp(`${bl}${l18}${Tb}${l70}${Tb}${l10}${Tb}${l14}${Tb}${l13}${br}`);
  log(tableBottom);
};

const printAnsiExplanation = () => {
  log(
    alp(`
          ------------ ANSI escape sequence formatting ------------         

The ANSI escape sequences are sequences of characters that are used to format and style text
in the terminal. They are often used to change the color of the text, background, or cursor,
or to move the cursor to a different position on the screen.

The basic format of an ANSI escape sequence is either ${fmt('\\e[<sequence>m', 2)}, or ${fmt(
      '\\033[<sequence>m',
      2
    )} where
${fmt('\\e[<sequence>m', 2)} or ${fmt(
      '\\[<sequence>m',
      2
    )} represents the escape character and ${fmt('<sequence>', 2)} is a series of
semicolon-separated values that define the specific formatting or styling that is being requested.
You'll have to determine which one is appropriate for your current environment. For example, bash
supports both, but in python you have to use ${fmt(
      '\\033[<sequence>m',
      2
    )}, and for Javascript you can only use
${fmt('\\e[<sequence>m', 2)} in template strings, but the ${fmt(
      '\\033[<sequence>m',
      2
    )} can be used in regular strings. What
you can use varies by language.

For example, to change the text color to red, the following sequence would be used:

${fmt('\\e[31m', 2)} OR ${fmt('\\033[31m', 2)}

The value 31 is the code for red text. To reset the text formatting back to the default, 
the following sequence is used:

${fmt('\\e[0m', 2)} OR ${fmt('\\033[0m', 2)}

So, to display text in red, you could use the following code:

${fmt('echo -e \\e[31mHello, World!\\e[0m', 2)}
=> ${fmt('Hello, World!', 0, 31)}

The order that the codes are entered doesn't matter as it is simply based off of 
the values. So all of these would produce the same result, bold red text with a yellow
background.

// eslint-disable-next-line quotes
${fmt("echo -e '\\e[1;31;43mHello World!\\e[0m'", 2)}
=> ${fmt('Hello, World!', 1, 31, 43)}

${fmt("echo -e '\\e[43;1;31mHello World!\\e[0m'", 2)}
=> ${fmt('Hello, World!', 43, 1, 31)}

${fmt("echo -e '\\e[31;1;43mHello World!\\e[0m'", 2)}
=> ${fmt('Hello, World!', 31, 1, 43)}

More Examples:

# Bold text

${fmt("echo -e '\\e[1mHello World\\e[0m", 2)}
=> ${fmt('Hello, World!', 1)}

# Bold text with green text

${fmt("echo -e '\\e[1;32mHello World\\e[0m'", 2)}
=> ${fmt('Hello, World!', 1, 32)}

# Black text with white background

${fmt("echo -e '\\e[7;37mHello World\\e[0m'", 2)}
=> ${fmt('Hello, World!', 7, 37)}
`)
  );
};

const styles = [0, 1, 2, 3, 4, 7, 9];
const foreground = [30, 31, 32, 33, 34, 35, 36, 37];
const background = [40, 41, 42, 43, 44, 45, 46, 47];
const alt_fg = [90, 91, 92, 93, 94, 95, 96, 97];

const printStyles = () => {
  for (let style of styles) {
    log(p5, fmt(`Example`, style));
  }
};

const printForeground = () => {
  for (let color of foreground) {
    log(p5, fmt(`Example - \\e[${color}m`, 0, color));
  }
};
const printBackground = () => {
  for (let color of background) {
    log(p5, fmt(`Example - \\e[${color}m`, 0, 0, color));
  }
};

const printAltFg = () => {
  for (let color of alt_fg) {
    log(p5, fmt(`Example - \\e[${color}m`, 0, color));
  }
};

const printAllBasic = () => {
  for (let i = 0; i < 8; i++) {
    const style = i === 5 || i === 6 ? 0 : i;
    // Fuck it, good enough.
    log(
      fmt(`Example: \\e[${style}m`, style),
      fmt(`Example: \\e[${foreground[i]}m`, foreground[i]),
      fmt(`Example: \\e[${alt_fg[i]}m`, alt_fg[i]),
      fmt(`Example: \\e[${background[i]}m`, background[i]),
      fmt(`Example: \\e[${foreground[i]};${background[i]}m`, foreground[i], background[i]),
      fmt(`Example: \\e[${alt_fg[i]};${background[i]}m`, alt_fg[i], background[i]),
      fmt(`Example: \\e[${style};${foreground[i]}m`, style, foreground[i]),
      fmt(`Example: \\e[${style};${alt_fg[i]}m`, style, alt_fg[i]),
      fmt(`Example: \\e[${style};${background[i]}m`, style, background[i]),
      fmt(
        `Example: \\e[${style};${foreground[i]};${background[i]}m`,
        style,
        foreground[i],
        background[i]
      ),
      fmt(`Example: \\e[${style};${alt_fg[i]};${background[i]}m`, style, alt_fg[i], background[i])
    );
  }
};

module.exports = {
  printAllBasic,
  printAltFg,
  printAnsiExplanation,
  printBackground,
  printColorTable,
  printForeground,
  printStyles,
};
