const borders = {
  Tb: '┴',
  Tl: '├',
  Tr: '┤',
  Tt: '┬',
  bl: '└',
  br: '┘',
  h: '─',
  t: '┼',
  tl: '┌',
  tr: '┐',
  v: '│',
};

const fmt = (str, style = 0, fg = '', bg = '') => {
  const reset = '\033[0m';
  const base_format = '\033[';
  const style_format = base_format + style;
  const fg_format = fg ? style_format + ';' + fg : style_format;
  const bg_format = bg ? fg_format + ';' + bg : fg_format;
  const format = bg_format + 'm';

  return format + str + reset;
};

const l = n => borders.h.repeat(n);
const p = n => ' '.repeat(n);

const paddings = {};
for (let i = 1; i < 100; i++) {
  paddings[`p${i}`] = p(i);
}

const lines = {};
for (let i = 1; i < 100; i++) {
  lines[`l${i}`] = l(i);
}

const addLeftPadding = (text, amount = 5) => {
  const textPadded = text
    .split('\n')
    .map(t => `${p(amount)}${t}`)
    .join('\n');
  return textPadded;
};

module.exports = {
  addLeftPadding,
  borders,
  fmt,
  l,
  lines,
  p,
  paddings,
};
